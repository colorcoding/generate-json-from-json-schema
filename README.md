This project helps in generating thousands of JSON test inputs to test a webservice property.

JSON schema should be fed to the program which will be parsed, and helps in building a custom object which is used by Jqwik property based testing framework to generate hundreds/ thousands of test inputs to test a property of webservice