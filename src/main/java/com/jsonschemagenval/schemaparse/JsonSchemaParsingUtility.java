package com.jsonschemagenval.schemaparse;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.jsonschemagenval.schemaelement.JSONSchemaElement;
import com.jsonschemagenval.schemaelement.JsonSchemaTagEnum;

public class JsonSchemaParsingUtility {

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static JSONSchemaElement parseJsonSchema(String filePath) {
		JSONSchemaElement parentNode = new JSONSchemaElement();
		try {
			JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));

			JsonParser parser = new JsonParser();
			JsonObject parentObject = parser.parse(reader).getAsJsonObject();
			// parentNode.setElementName( JsonSchemaTagEnum.ROOT_NODE.name() );
			parentNode.setHasChildren(true);
			parentNode.setElementType(JsonSchemaTagEnum.ELEMENT_TYPE.COMPLEX_TYPE);
			parseJSONSchema(parentObject, parentNode);
			// System.out.println( "After Parsing element->" + parentNode );
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return parentNode;
	}

	/**
	 * 
	 * @param parentObject
	 * @param parentlement
	 */
	private static void parseJSONSchema(JsonObject parentObject, JSONSchemaElement parentlement) {

		Set<Map.Entry<String, JsonElement>> entries = parentObject.entrySet();
		for (Map.Entry<String, JsonElement> entry : entries) {
			JSONSchemaElement element = new JSONSchemaElement();
			// System.out.println("Entry Key->" + entry.getKey());
			// System.out.println("Entry VALUE->" + entry.getValue());
			if (entry.getValue().isJsonObject()) {
				String key = entry.getKey();
				element.setElementName(key.toUpperCase());
				element.setElementType(JsonSchemaTagEnum.ELEMENT_TYPE.COMPLEX_TYPE);
				// element.setHasChildren(true);
				setPrimitiveType(entry, element);
				/**
				 * create child
				 */
				parseJSONSchema(entry.getValue().getAsJsonObject(), element);
				/**
				 * Pull Element from bottom to this key=> items->properties->actual element as
				 * ITEMS,PROPERTIES TAGS of JSON is not required
				 */
				element = pullChildFromBottomToTop(parentlement, element, key);

			} else {
				if (!"type".equalsIgnoreCase(entry.getKey())) {

					if ("minimum".equalsIgnoreCase(entry.getKey())) {
						if (entry.getValue().isJsonPrimitive()) {
							parentlement.setMin(entry.getValue().getAsString());
						} else if (entry.getValue().isJsonNull()) {
							parentlement.setMin(entry.getValue().getAsJsonNull().toString());
						}
					} else if ("maximum".equalsIgnoreCase(entry.getKey())) {
						if (entry.getValue().isJsonPrimitive()) {
							parentlement.setMax(entry.getValue().getAsString());
						} else if (entry.getValue().isJsonNull()) {
							parentlement.setMax(entry.getValue().getAsJsonNull().toString());
						}
					} else if ("minLength".equalsIgnoreCase(entry.getKey())) {
						if (entry.getValue().isJsonPrimitive()) {
							parentlement.setMin(entry.getValue().getAsString());
						} else if (entry.getValue().isJsonNull()) {
							parentlement.setMin(entry.getValue().getAsJsonNull().toString());
						}
					} else if ("maxLength".equalsIgnoreCase(entry.getKey())) {
						if (entry.getValue().isJsonPrimitive()) {
							parentlement.setMax(entry.getValue().getAsString());
						} else if (entry.getValue().isJsonNull()) {
							parentlement.setMax(entry.getValue().getAsJsonNull().toString());
						}
					} else if ("default".equalsIgnoreCase(entry.getKey())) {
						if (entry.getValue().isJsonPrimitive()) {
							parentlement.setDefaultVal(entry.getValue().getAsString());
						} else if (entry.getValue().isJsonNull()) {
							parentlement.setDefaultVal(entry.getValue().getAsJsonNull().toString());
						}
					} //TODO: Write blocks for Pattern and Examples also 
					else {
						element.setElementName(entry.getKey().toUpperCase());
						element.setElementType(JsonSchemaTagEnum.ELEMENT_TYPE.SIMPLE_TYPE);
						if (entry.getValue().isJsonArray()) {
							element.setElementValue(entry.getValue().getAsJsonArray().toString());
						} else if (entry.getValue().isJsonPrimitive()) {
							element.setElementValue(entry.getValue().getAsString());
						} else if (entry.getValue().isJsonNull()) {
							element.setElementValue(entry.getValue().getAsJsonNull().toString());
						}
						element.setHasChildren(false);
					}
				}
			}
			if (null != element && null != element.getElementName() && element.getElementName().length() > 0) {
				parentlement.addChildElement(element);
			}
		}
	}

	/**
	 * 
	 * @param parentlement
	 * @param element
	 * @param key
	 * @return
	 */
	private static JSONSchemaElement pullChildFromBottomToTop(JSONSchemaElement parentlement, JSONSchemaElement element,
			String key) {
		if (JsonSchemaTagEnum.PROPERTIES.name().equalsIgnoreCase(key)
				|| JsonSchemaTagEnum.ITEMS.name().equalsIgnoreCase(key)) {
			// System.out.println("For Properties Type key->" + element);
			parentlement.setChildren(element.getChildren());
			element.setChildren(null);
			if (JsonSchemaTagEnum.ITEMS.name().equalsIgnoreCase(key)) {
				if (null == parentlement.getMin()) {
					parentlement.setMin(element.getMin());
				}
				if (null == parentlement.getMax()) {
					parentlement.setMax(element.getMax());
				}
			}
			element = null;

		}
		return element;
	}

	/**
	 * 
	 * @param entry
	 * @param element
	 */
	private static void setPrimitiveType(Map.Entry<String, JsonElement> entry, JSONSchemaElement element) {
		Gson gson = new Gson();
		String jsonTypeCasted = gson.toJson(entry.getValue());
		String formattedJsonString = (((jsonTypeCasted.replaceAll("\\{", "").trim()).replaceAll("\\}", "").trim())
				.replaceAll("\"", "")).trim();
		String[] splitted = formattedJsonString.split(",");
		if (splitted.length >= 2) {
			String type = (!StringUtils.isEmpty(splitted[1]) ? splitted[1].split(":")[1] : "");
			String validPrimitiveType = isValidJsonPrimitiveDataType(type);
			if (validPrimitiveType != null) {
				element.setElementValue(validPrimitiveType.toUpperCase());
				element.setElementType(JsonSchemaTagEnum.ELEMENT_TYPE.SIMPLE_TYPE);
				element.setHasChildren(false);
			}
		}
		gson = null;
	}

	/**
	 * 
	 * @param inputPrimitiveType
	 * @return
	 */
	private static String isValidJsonPrimitiveDataType(String inputPrimitiveType) {
		String jsonPrimitiveDataType = null;
		if ("string".equalsIgnoreCase(inputPrimitiveType)) {
			jsonPrimitiveDataType = "string";
		} else if ("number".equalsIgnoreCase(inputPrimitiveType)) {
			jsonPrimitiveDataType = "number";
		} else if ("boolean".equalsIgnoreCase(inputPrimitiveType)) {
			jsonPrimitiveDataType = "boolean";
		} else if ("integer".equalsIgnoreCase(inputPrimitiveType)) {
			jsonPrimitiveDataType = "integer";
		} else {
			// System.out.println("This Data Type Is Not Supproted in JSON->" +
			// inputPrimitiveType);
			jsonPrimitiveDataType = null;
		}

		return jsonPrimitiveDataType;
	}

}
