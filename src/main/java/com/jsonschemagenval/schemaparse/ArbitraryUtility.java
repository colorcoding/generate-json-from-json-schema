package com.jsonschemagenval.schemaparse;

import com.jsonschemagenval.schemaelement.JSONSchemaElement;

import net.jqwik.api.Arbitraries;
import net.jqwik.api.Arbitrary;
import net.jqwik.providers.BooleanArbitraryProvider;

public class ArbitraryUtility {

	public static Arbitrary<String> getStringArbitrary(JSONSchemaElement el) {
		Arbitrary<String> res = null;
		
		// First check if there are examples given which will form the set to choose
		if(el.getExamples() != null) {
			res = Arbitraries.strings().withSamples(el.getExamples());
		}
		
		// Variations for min, max and Pattern (we don't have anything for pattern yet)
		if(el.getMax() != null && el.getMin() != null && el.getPattern() != null) { // TODO: for pattern
			res = Arbitraries.strings().ofMinLength(Integer.parseInt(el.getMin())).ofMaxLength(Integer.parseInt(el.getMax()));
		} else if (el.getMax() != null  && el.getMin() != null && el.getPattern() == null) {
			res = Arbitraries.strings().ascii().ofMinLength(Integer.parseInt(el.getMin())).ofMaxLength(Integer.parseInt(el.getMax()));
		} else if (el.getMax() != null  && el.getMin() == null && el.getPattern() == null) {
			res = Arbitraries.strings().ascii().ofMaxLength(Integer.parseInt(el.getMax()));
		} else if (el.getMax() != null  && el.getMin() == null && el.getPattern() == null) {
			res = Arbitraries.strings().ascii().ofMaxLength(Integer.parseInt(el.getMax()));
		} else if (el.getMax() == null  && el.getMin() == null && el.getPattern() == null) {
			res = Arbitraries.strings().ascii();
		} else if (el.getMax() == null  && el.getMin() == null && el.getPattern() != null) {  // TODO: for pattern
			res = Arbitraries.strings().ascii();
		} else if (el.getMax() == null  && el.getMin() != null && el.getPattern() != null) { // TODO: for pattern
			res = Arbitraries.strings().ofMinLength(Integer.parseInt(el.getMax()));
		} else if (el.getMax() == null  && el.getMin() != null && el.getPattern() == null) {
			res = Arbitraries.strings().ascii().ofMinLength(Integer.parseInt(el.getMax()));
		}
		
		// res is null and Default is available in Schema
		if(res == null && el.getDefaultVal() != null) {
			res = Arbitraries.strings().withSamples(el.getDefaultVal());
		}
		return res;
	}
	
	public static Arbitrary<Integer> getIntegerArbitrary(JSONSchemaElement el) {
		Arbitrary<Integer> res = null;
		if(el.getMin() != null && el.getMax() != null) 
			res = Arbitraries.integers().between(Integer.parseInt(el.getMin()), Integer.parseInt(el.getMax()));
		else
			res = Arbitraries.integers();
		return res;
	}
	
	public static Arbitrary<Double> getDoubleArbitrary(JSONSchemaElement el) {
		Arbitrary<Double> res = null;
		if(el.getMin() != null && el.getMax() != null) 
			res = Arbitraries.doubles().between(Double.parseDouble(el.getMin()), Double.parseDouble(el.getMax()));
		else
			res = Arbitraries.doubles();
		return res;
	}
	
	public static Arbitrary<Float> getFloatArbitrary(JSONSchemaElement el) {
		Arbitrary<Float> res = null;
		if(el.getMin() != null && el.getMax() != null) 
			res = Arbitraries.floats().between(Float.parseFloat(el.getMin()), Float.parseFloat(el.getMax()));
		else
			res = Arbitraries.floats();
		return res;
	}
	
	public static  Arbitrary<Boolean> getBooleanArbitrary(JSONSchemaElement el) {
		Arbitrary<Boolean> res = null;
		res = Arbitraries.of(true, false);
		return res;
	}
	
}
