package com.jsonschemagenval.schemaelement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONSchemaElement {

	private String elementName;
	private JsonSchemaTagEnum.ELEMENT_TYPE elementType;
	private String elementValue;
	private String min;
	private String max;
	private String pattern;
	private String defaultVal;
	private String[] examples;
	private boolean hasChildren;
	private List<JSONSchemaElement> children = new ArrayList<>();



	public void addChildElement(JSONSchemaElement childElement) {
	    children.add( childElement );
	}

	
	/**
	 * @return the elementName
	 */
	public String getElementName() {
	    return elementName;
	}

	/**
	 * @param elementName
	 *            the elementName to set
	 */
	public void setElementName(String elementName) {
	    this.elementName = elementName;
	}

	/**
	 * @return the elementType
	 */
	public JsonSchemaTagEnum.ELEMENT_TYPE getElementType() {
	    return elementType;
	}

	/**
	 * @param elementType
	 *            the elementType to set
	 */
	public void setElementType(JsonSchemaTagEnum.ELEMENT_TYPE elementType) {
	    this.elementType = elementType;
	}

	/**
	 * @return the elementValue
	 */
	public String getElementValue() {
	    return elementValue;
	}

	/**
	 * @param elementValue
	 *            the elementValue to set
	 */
	public void setElementValue(String elementValue) {
	    this.elementValue = elementValue;
	}

	/**
	 * @return the minItems
	 */
	public String getMin() {
	    return min;
	}

	/**
	 * @param minItems
	 *            the minItems to set
	 */
	public void setMin(String min) {
	    this.min = min;
	}

	/**
	 * @return the maxItems
	 */
	public String getMax() {
	    return max;
	}

	/**
	 * @param maxItems
	 *            the maxItems to set
	 */
	public void setMax(String max) {
	    this.max = max;
	}

	/**
	 * @return the hasChildren
	 */
	public boolean isHasChildren() {
	    return hasChildren;
	}

	/**
	 * @param hasChildren
	 *            the hasChildren to set
	 */
	public void setHasChildren(boolean hasChildren) {
	    this.hasChildren = hasChildren;
	}

	/**
	 * @return the children
	 */
	public List<JSONSchemaElement> getChildren() {
	    return children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(List<JSONSchemaElement> children) {
	    this.children = children;
	}
	
	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getDefaultVal() {
		return defaultVal;
	}

	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}

	public String[] getExamples() {
		return examples;
	}

	public void setExamples(String[] examples) {
		this.examples = examples;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((defaultVal == null) ? 0 : defaultVal.hashCode());
		result = prime * result + ((elementName == null) ? 0 : elementName.hashCode());
		result = prime * result + ((elementType == null) ? 0 : elementType.hashCode());
		result = prime * result + ((elementValue == null) ? 0 : elementValue.hashCode());
		result = prime * result + Arrays.hashCode(examples);
		result = prime * result + (hasChildren ? 1231 : 1237);
		result = prime * result + ((max == null) ? 0 : max.hashCode());
		result = prime * result + ((min == null) ? 0 : min.hashCode());
		result = prime * result + ((pattern == null) ? 0 : pattern.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JSONSchemaElement other = (JSONSchemaElement) obj;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (defaultVal == null) {
			if (other.defaultVal != null)
				return false;
		} else if (!defaultVal.equals(other.defaultVal))
			return false;
		if (elementName == null) {
			if (other.elementName != null)
				return false;
		} else if (!elementName.equals(other.elementName))
			return false;
		if (elementType != other.elementType)
			return false;
		if (elementValue == null) {
			if (other.elementValue != null)
				return false;
		} else if (!elementValue.equals(other.elementValue))
			return false;
		if (!Arrays.equals(examples, other.examples))
			return false;
		if (hasChildren != other.hasChildren)
			return false;
		if (max == null) {
			if (other.max != null)
				return false;
		} else if (!max.equals(other.max))
			return false;
		if (min == null) {
			if (other.min != null)
				return false;
		} else if (!min.equals(other.min))
			return false;
		if (pattern == null) {
			if (other.pattern != null)
				return false;
		} else if (!pattern.equals(other.pattern))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "JSONSchemaElement [elementName=" + elementName + ", elementType=" + elementType + ", elementValue="
				+ elementValue + ", min=" + min + ", max=" + max + ", pattern=" + pattern + ", defaultVal=" + defaultVal
				+ ", examples=" + Arrays.toString(examples) + ", hasChildren=" + hasChildren + ", children=" + children
				+ "]";
	}
	
}
