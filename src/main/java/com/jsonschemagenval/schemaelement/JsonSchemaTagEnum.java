package com.jsonschemagenval.schemaelement;

public enum JsonSchemaTagEnum {

	PROPERTIES, ITEMS;

	public enum ELEMENT_TYPE {
		SIMPLE_TYPE, COMPLEX_TYPE

	}
}
