package com.jsonschemagenval.combinator;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import net.jqwik.api.Arbitrary;
import net.jqwik.properties.shrinking.*;
import net.jqwik.api.Shrinkable;
import net.jqwik.api.RandomGenerator;

public class SchemaCombinator {
	
	public static ListCombinator combine(List<Arbitrary> listOfArbitraries) {
		return new ListCombinator(listOfArbitraries);
	}

	public static class ListCombinator {
		private final List<Arbitrary> listOfArbitraries;

		private ListCombinator(List<Arbitrary> listOfArbitraries) {
			this.listOfArbitraries = listOfArbitraries;
		}

		@SuppressWarnings("unchecked")
		public <R> Arbitrary<R> as(Function<List, R> combinator) {
			return (genSize) -> {
				List<RandomGenerator> listOfGenerators = listOfArbitraries
					.stream()
					.map(a -> a.generator(genSize))
					.collect(Collectors.toList());

				return random -> {
					List<Shrinkable<Object>> shrinkables = listOfGenerators
						.stream()
						.map(g -> g.next(random))
						.map(s -> (Shrinkable<Object>) s)
						.collect(Collectors.toList());

					Function<List<Object>, R> combineFunction = params -> combinator.apply((List) params);

					return new CombinedShrinkable<>(shrinkables, combineFunction);
				};
			};
		}
	}
		
}
