package com.jsonschemagenval.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.jsonschemagenval.combinator.SchemaCombinator;
import com.jsonschemagenval.schemaelement.JSONSchemaElement;
import com.jsonschemagenval.schemaparse.ArbitraryUtility;
import com.jsonschemagenval.schemaparse.JsonSchemaParsingUtility;

import net.jqwik.api.Arbitraries;
import net.jqwik.api.Arbitrary;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.Provide;
import net.jqwik.properties.arbitraries.ListArbitrary;
import net.jqwik.providers.BooleanArbitraryProvider;
import net.jqwik.providers.ListArbitraryProvider;

public class SchemaGenTest {

	public static JSONSchemaElement SCHEMA_EL = JsonSchemaParsingUtility
			.parseJsonSchema("D:\\Practice\\sts-workspace\\userSchema.json");

	public void initialize() {
		SCHEMA_EL = JsonSchemaParsingUtility.parseJsonSchema("D:\\Practice\\sts-workspace\\userSchema.json");
		System.out.println(SCHEMA_EL.toString());
	}

	public Arbitrary getArbitraryOfSchemaEl(JSONSchemaElement el) {
		Arbitrary resultArb = null;
		// WRITE A SWITCH CASE
		if (el.getElementValue().equals("STRING")) {
			resultArb = ArbitraryUtility.getStringArbitrary(el);
		} else if (el.getElementValue().equals("NUMBER")) {
			resultArb = ArbitraryUtility.getDoubleArbitrary(el);
		} else if (el.getElementValue().equals("INTEGER")) {
			resultArb = ArbitraryUtility.getIntegerArbitrary(el);
		} else if (el.getElementValue().equals("BOOLEAN")) {
			resultArb = ArbitraryUtility.getBooleanArbitrary(el);
		}
		return resultArb;
	}

	// Instead of generating User objects, generate HashMap
	@Provide
	public Arbitrary<Map<String, Object>> validUsers() {
		List<String> keys = new ArrayList<>();
		List finalArbList = new ArrayList();
		finalArbList = getArbitraryList(keys, SCHEMA_EL, finalArbList);
		MapExtender m = new MapExtender();
		return SchemaCombinator.combine(finalArbList).as((l2) -> m.putValuesAndReturn(keys, l2));
	}

	private List getArbitraryList(List<String> keys, JSONSchemaElement schemaEl, List finalArbList) {
		for (JSONSchemaElement el : schemaEl.getChildren()) { // PLEASE USE STREAMS API
			if (el.isHasChildren() && el.getElementType().equals("COMPLEX_TYPE")) {
				List lnested = getArbitraryList(new ArrayList<String>(), el, new ArrayList());
				finalArbList.add(lnested);
			} else { // SIMPLE_TYPE
				Arbitrary arb = getArbitraryOfSchemaEl(el);
				if (arb != null) {
					keys.add(el.getElementName());
					finalArbList.add(arb);
				}
			}
		}
		return finalArbList;
	}

	@SuppressWarnings("hiding")
	public class MapExtender extends HashMap {
		private static final long serialVersionUID = 1L;
		private Map<String, Object> mapObj = new HashMap<>();
		public Map putValuesAndReturn(List<String> keys, List l) {
			for (int i = 0; i < l.size(); i++) { // PLEASE USE STREAMS API
				mapObj.put(keys.get(i), l.get(i));
			}
			return mapObj;
		}
	}

	@Property
	public void testHashMapGenerator(@ForAll Map<String, Object> map) {
		System.out.println("User generated is : " + map.toString());
		Assertions.assertThat(map.get("age")).isEqualTo("23");
	}
}
