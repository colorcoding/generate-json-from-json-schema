package com.jsonschemagenval.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.jsonschemagenval.combinator.SchemaCombinator;
import com.jsonschemagenval.schemaelement.JSONSchemaElement;
import com.jsonschemagenval.schemaparse.ArbitraryUtility;
import com.jsonschemagenval.schemaparse.JsonSchemaParsingUtility;
import com.jsonschemagenval.test.SchemaGenTest.MapExtender;

import net.jqwik.api.Arbitraries;
import net.jqwik.api.Arbitrary;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.Provide;
import net.jqwik.properties.arbitraries.ListArbitrary;
import net.jqwik.providers.BooleanArbitraryProvider;
import net.jqwik.providers.ListArbitraryProvider;

public class SchemaGenTestCopy {

	public static JSONSchemaElement SCHEMA_EL = JsonSchemaParsingUtility.parseJsonSchema("D:\\Practice\\sts-workspace\\userSchema.json");

	public void initialize() {
		SCHEMA_EL = JsonSchemaParsingUtility.parseJsonSchema("D:\\Practice\\sts-workspace\\userSchema.json");
		System.out.println(SCHEMA_EL.toString());
	}
	
	public Arbitrary getArbitraryOfSchemaEl(JSONSchemaElement el) {
	
		Arbitrary resultArb = null;
		
		if(el.getElementType().equals("COMPLEX_TYPE")) {
//			// Age
//			Arbitrary<Integer> ages = Arbitraries.integers().between(25, 55);
//			// Salary
//			Arbitrary<Double> salarys = Arbitraries.doubles().between(10000, 100000);
//			
//			List<Arbitrary> l11 = new ArrayList<Arbitrary>();
//			l11.add(ages);
//			l11.add(salarys);
//			
//			List<Arbitrary> finalArb = new ArrayList<Arbitrary>();
//			
//			resultArb = SchemaCombinator.combine(l11).as((l2) -> m.putValuesAndReturn(keys, l2));
			
		} else { //SIMPLE TYPE
			if(el.getElementValue().equals("STRING")) {
				resultArb = Arbitraries.strings().withCharRange('a', 'z')
				        .ofMinLength(4).ofMaxLength(12);
			} else if (el.getElementValue().equals("NUMBER")) {
				resultArb = Arbitraries.doubles().between(10000, 100000);
			} else if (el.getElementValue().equals("INTEGER")) {
				resultArb = Arbitraries.integers().between(0, 30);
			} else if (el.getElementValue().equals("BOOLEAN")) {
				
			}
		}
		return resultArb;
	}
 
	// Instead of generating User objects, generate HashMap
	@Provide
	public Arbitrary<Map<String, Object>> validUsers() {
//		// Age
//		Arbitrary<Integer> ages = Arbitraries.integers().between(25, 55);
//		// Salary
//		Arbitrary<Double> salarys = Arbitraries.doubles().between(10000, 100000);
//		// YrsExp
//		Arbitrary<Integer> exps = Arbitraries.integers().between(0, 30);
//		// Aadhaar
//		Arbitrary<Long> aadhaars = Arbitraries.longs().between(000000000000L, 999999999999L);
		
		List<String> keys = new ArrayList<>();
		List l = new ArrayList();
		
		for(JSONSchemaElement el : SCHEMA_EL.getChildren()) {
			Arbitrary arb = getArbitraryOfSchemaEl(el);
			if(arb != null) {
				keys.add(el.getElementName());
				l.add(arb);
			}
		}
		
//		keys.add("age");
//		keys.add("salary");
//		keys.add("exp");
//		keys.add("aadhaar");
//
//		
//		l.add(ages);
//		l.add(salarys);
//		l.add(exps);
//		l.add(aadhaars);

		MapExtender m = new MapExtender();

		return SchemaCombinator.combine(l).as((l2) -> m.putValuesAndReturn(keys, l2));
	}
	
//	public Arbitrary getArbitraryOfSchemaEl(JSONSchemaElement el) {
//		Arbitrary resultArb = null;
//		// WRITE A SWITCH CASE
//		if (el.getElementValue().equals("STRING")) {
//			resultArb = ArbitraryUtility.getStringArbitrary(el);
//		} else if (el.getElementValue().equals("NUMBER")) {
//			resultArb = ArbitraryUtility.getDoubleArbitrary(el);
//		} else if (el.getElementValue().equals("INTEGER")) {
//			resultArb = ArbitraryUtility.getIntegerArbitrary(el);
//		} else if (el.getElementValue().equals("BOOLEAN")) {
//			resultArb = ArbitraryUtility.getBooleanArbitrary(el);
//		} else {
//			// Age
//			Arbitrary<Integer> ages = Arbitraries.integers().between(25, 55);
//			// Salary
//			Arbitrary<Double> salarys = Arbitraries.doubles().between(10000, 100000);
//
//			List<Arbitrary> l11 = new ArrayList<Arbitrary>();
//			l11.add(ages);
//			l11.add(salarys);
//
//			List<String> keys = new ArrayList<String>();
//			keys.add("innerage");
//			keys.add("innersalary");
//			MapExtender m = new MapExtender(); // PLEASE WRITE A LAMBDA HERE
//			resultArb = SchemaCombinator.combine(l11).as((l2) -> m.putValuesAndReturn(keys, l2));
//		}
//		return resultArb;
//	}

	@SuppressWarnings("hiding")
	public class MapExtender extends HashMap {
		private static final long serialVersionUID = 1L;

		private Map<String, Object> mapObj = new HashMap<>();

		public Map putValuesAndReturn(List<String> keys, List l) {
			for (int i = 0; i < l.size(); i++) {
				mapObj.put(keys.get(i), l.get(i));
			}
			return mapObj;
		}

	}

	@Property
	public void testHashMapGenerator(@ForAll Map<String, Object> map) {
		//
		System.out.println("User generated is : " + map.toString());
		Assertions.assertThat(map.get("age")).isEqualTo("23");
	}
}
